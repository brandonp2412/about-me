import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DockerComponent } from './docker.component';

import dockerfile from 'highlight.js/lib/languages/dockerfile';
import nginx from 'highlight.js/lib/languages/nginx';
import { HighlightModule } from 'ngx-highlightjs';
import { ClipboardModule } from 'ngx-clipboard';
import { CicdComponent } from '../cicd/cicd.component';
import { MatIconModule, MatSnackBarModule } from '@angular/material';

function hljsLanguages() {
  return [
    { name: 'dockerfile', func: dockerfile },
    { name: 'nginx', func: nginx }
  ];
}

describe('DockerComponent', () => {
  let component: DockerComponent;
  let fixture: ComponentFixture<DockerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DockerComponent, CicdComponent],
      imports: [
        HighlightModule.forRoot({ languages: hljsLanguages }),
        ClipboardModule,
        MatIconModule,
        MatSnackBarModule
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DockerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

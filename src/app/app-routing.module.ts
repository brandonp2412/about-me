import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ProjectsComponent } from './projects/projects.component';
import { TechnologiesComponent } from './technologies/technologies.component';
import { ReportBugsComponent } from './report-bugs/report-bugs.component';
import { CreationBlogComponent } from './creation-blog/creation-blog.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'projects', component: ProjectsComponent},
  {path: 'technologies', component: TechnologiesComponent},
  {path: 'report-bugs', component: ReportBugsComponent},
  {path: 'creation-blog', component: CreationBlogComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }

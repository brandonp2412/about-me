import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GettingStartedComponent } from './getting-started.component';
import { HighlightModule } from 'ngx-highlightjs';
import yaml from 'highlight.js/lib/languages/yaml';
import typescript from 'highlight.js/lib/languages/typescript';
import { MatIconModule, MatSnackBarModule } from '@angular/material';
import { ClipboardModule } from 'ngx-clipboard';

function hljsLanguages() {
  return [
    { name: 'typescript', func: typescript },
    { name: 'yaml', func: yaml }
  ];
}

describe('GettingStartedComponent', () => {
  let component: GettingStartedComponent;
  let fixture: ComponentFixture<GettingStartedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GettingStartedComponent],
      imports: [
        HighlightModule.forRoot({ languages: hljsLanguages }),
        MatIconModule,
        ClipboardModule,
        MatSnackBarModule
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GettingStartedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

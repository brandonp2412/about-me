import { Component, OnInit } from '@angular/core';
import { SnackbarService } from 'src/app/snackbar.service';

@Component({
  selector: 'app-getting-started',
  templateUrl: './getting-started.component.html',
  styleUrls: ['./getting-started.component.css']
})
export class GettingStartedComponent {
  draft1 = `build:
  script:
  - npm i
  - npm run build
  artifacts:
    paths:
    - dist/about-me
  only:
  - master
`;

  draft2 = `image: node:latest

pages:
  script:
  - npm i
  - npm run build
  - mv dist/about-me/* public/
  artifacts:
    paths:
    - public
  only:
  - master
  `;

  success = `image: node:latest

cache:
  key: \${CI_COMMIT_REF_SLUG}
  paths:
  - node_modules/

pages:
  script:
  - npm i
  - npm run build
  - mv dist/about-me/ public/
  artifacts:
    paths:
    - public
  only:
  - master
`;

  constructor(public snackbarService: SnackbarService) {}
}

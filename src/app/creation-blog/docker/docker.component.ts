import { Component, OnInit } from '@angular/core';
import { SnackbarService } from 'src/app/snackbar.service';

@Component({
  selector: 'app-docker',
  templateUrl: './docker.component.html',
  styleUrls: ['./docker.component.css']
})
export class DockerComponent {
  docker = `FROM node:10 AS base
WORKDIR /app/
COPY . .
RUN npm install && npm run build:prod

FROM nginx:alpine
COPY --from=base /app/dist/about-me/ /usr/share/nginx/html/
COPY default.conf /etc/nginx/conf.d/default.conf
EXPOSE 80`;

  nginx = `server {
    listen       80;
    root   /usr/share/nginx/html;
    index  index.html index.htm;

    location / {
        try_files $uri $uri/ /index.html;
    }

    location = /50x.html {
        root   /usr/share/nginx/html;
    }
}
`;
  constructor(public snackbarService: SnackbarService) {}
}

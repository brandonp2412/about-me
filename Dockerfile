FROM node:10 AS base
WORKDIR /app/
COPY . .
RUN npm install && npm run build:prod

FROM nginx:alpine
COPY --from=base /app/dist/about-me/ /usr/share/nginx/html/
COPY default.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
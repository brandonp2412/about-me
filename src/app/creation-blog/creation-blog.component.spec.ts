import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreationBlogComponent } from './creation-blog.component';
import { GettingStartedComponent } from './getting-started/getting-started.component';
import { MatTabsModule, MatCardModule, _MatCheckboxRequiredValidatorModule, MatIconModule, MatSnackBarModule } from '@angular/material';
import yaml from 'highlight.js/lib/languages/yaml';
import typescript from 'highlight.js/lib/languages/typescript';
import { HighlightModule } from 'ngx-highlightjs';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { DockerComponent } from './docker/docker.component';
import { CicdComponent } from './cicd/cicd.component';
import { ClipboardModule } from 'ngx-clipboard';

function hljsLanguages() {
  return [
    { name: 'typescript', func: typescript },
    { name: 'yaml', func: yaml }
  ];
}

describe('CreationBlogComponent', () => {
  let component: CreationBlogComponent;
  let fixture: ComponentFixture<CreationBlogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreationBlogComponent, GettingStartedComponent, DockerComponent, CicdComponent],
      imports: [
        NoopAnimationsModule,
        MatTabsModule,
        MatCardModule,
        HighlightModule.forRoot({ languages: hljsLanguages }),
        ClipboardModule,
        MatIconModule,
        MatSnackBarModule
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreationBlogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

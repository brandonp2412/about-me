import { Component, OnInit } from '@angular/core';
import { SnackbarService } from 'src/app/snackbar.service';

@Component({
  selector: 'app-cicd',
  templateUrl: './cicd.component.html',
  styleUrls: ['./cicd.component.css']
})
export class CicdComponent {
  tests = `tests:
  stage: test
  cache:
    paths:
      - node_modules/
  script:
    # install dependencies to use chrome w/ puppeteer
    - apt update && apt install -yq gconf-service libasound2 libatk1.0-0 libc6 libcairo2 libcups2 \
      libdbus-1-3 libexpat1 libfontconfig1 libgcc1 libgconf-2-4 libgdk-pixbuf2.0-0 libglib2.0-0 \
      libgtk-3-0 libnspr4 libpango-1.0-0 libpangocairo-1.0-0 libstdc++6 libx11-6 libx11-xcb1 libxcb1 \
      libxcomposite1 libxcursor1 libxdamage1 libxext6 libxfixes3 libxi6 libxrandr2 libxrender1 libxss1 \
      libxtst6 ca-certificates fonts-liberation libappindicator1 libnss3 lsb-release xdg-utils wget
    - npm run test:ci
    - npm run e2e:ci
`;

  success = `image: node:latest

stages:
  - test
  - lint
  - build

tests:
  image: registry.gitlab.com/brandonp2412/nodejs-chromium:d89b7ee3
  stage: test
  script:
    - npm install
    - npm run test:ci
    - npm run e2e:ci

lint:
  stage: lint
  script:
    - npm install
    - npm run lint

pages:
  stage: build
  script:
    - npm install
    - npm run build:gitlab
    - mv dist/about-me/ public/
  artifacts:
    paths:
    - public
  only:
  - master
`;

  constructor(public snackbarService: SnackbarService) {}
}

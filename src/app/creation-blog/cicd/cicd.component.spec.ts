import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CicdComponent } from './cicd.component';
import { ClipboardModule } from 'ngx-clipboard';
import { MatIconModule, MatSnackBarModule } from '@angular/material';
import yaml from 'highlight.js/lib/languages/yaml';
import { HighlightModule } from 'ngx-highlightjs';

function hljsLanguages() {
  return [
    { name: 'yaml', func: yaml }
  ];
}

describe('CicdComponent', () => {
  let component: CicdComponent;
  let fixture: ComponentFixture<CicdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CicdComponent ],
      imports: [
        HighlightModule.forRoot({ languages: hljsLanguages }),
        ClipboardModule,
        MatIconModule,
        MatSnackBarModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CicdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
